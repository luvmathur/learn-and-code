import unittest
from MatrixMainClass import MatrixViewSet


class TestMatrixViewSet(unittest.TestCase):
    def __init__(self, *args):
        super(TestMatrixViewSet, self).__init__(*args)
        self.add_alternate_elements = MatrixViewSet()
        self.add_alternate_elements.array_elements = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

    def test_check_elements_of_array(self):
        array = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
        self.assertEqual(self.add_alternate_elements.array_elements[0][0], 1)
        self.assertNotEqual(self.add_alternate_elements.array_elements[0][2], 8)
        self.assertEqual(self.add_alternate_elements.array_elements[1][1], 5)
        self.assertEqual(self.add_alternate_elements.array_elements[2][2], 9)
        self.assertNotEqual(self.add_alternate_elements.array_elements[2][0], 4)
        self.assertEqual(self.add_alternate_elements.array_element, array)

    def test_calculate_even_indexes_sum(self):
        self.assertEqual(
            self.add_alternate_elements.calculate_even_indexes_sum(self.add_alternate_elements.array_elements),
            25)
        self.assertNotEqual(
            self.add_alternate_elements.calculate_even_indexes_sum(self.add_alternate_elements.array_elements), 20)

    def test_calculate_odd_indexes_sum(self):
        self.assertEqual(
            self.add_alternate_elements.calculate_odd_indexes_sum(self.add_alternate_elements.array_elements),
            20)
        self.assertNotEqual(
            self.add_alternate_elements.calculate_even_indexes_sum(self.add_alternate_elements.array_elements), 25)


if __name__ == '__main__':
    unittest.main()
