#include<stdio.h>
#include<stdlib.h>
# define null '\0'

typedef struct node{
	int popularity;
	struct node *next;
	struct node *prev;
}node;
struct node *head = null;
struct node *pointer = null;

struct node *insert(struct node *head , int popularity){
	struct node *tmp = malloc(sizeof(node));
	tmp->popularity = popularity;
	tmp->next = null;
	tmp->prev =null;
	if(head == null){
		head = tmp;
		pointer = head;
	}
	else{
		pointer->next = tmp;
		tmp->prev = pointer;
		pointer=tmp;
	}
	return head;
}
int delete_count =0;
struct node *algorithm_delete(struct node *head,int friends_to_delete){
	if(head){
		struct node *friend = head;
		struct node *last = friend;
		while(friend != null){
			if(friend->next){
				if(friend->popularity < friend->next->popularity){
					delete_count++;
					if(friend->prev == null){
						friend = friend->next;
						head = friend;
						friend->prev = null;
					}else{
						struct node *tmp = friend;
						friend->prev->next = friend->next;
						friend->next->prev = friend->prev;
						friend = friend->prev;
						tmp->next = null;
						tmp->prev = null;
					}
				}else{
					friend = friend->next;
				}
			}else{
				last = friend;
				break;
			}
			if(delete_count == friends_to_delete){
				break;
			}
		}
		int remaining = friends_to_delete-delete_count;
		while(remaining!=0){
			friend = friend->prev;
			friend->next = null;
			remaining--;
		}
	}
	return head;
}
void display(struct node *head){
	struct node *friend = head;
	while(friend!=null){
		printf("%d ",friend->popularity);
		friend = friend->next;
	}
	printf("\n");
}

int main(){

	int number_of_test_cases;
	scanf("%d",&number_of_test_cases);
	while(number_of_test_cases!=0){
		int facebook_friends,friends_to_delete,number_of_facebook_friend,popularity_score;
		scanf("%d%d",&facebook_friends,&friends_to_delete);
		for(number_of_facebook_friend=0;number_of_facebook_friend<facebook_friends;number_of_facebook_friend++){
			scanf("%d",&popularity_score);
			head = insert(head,popularity_score);
		}
		head = algorithm_delete(head,friends_to_delete);

		display(head);
		number_of_test_cases--;
		head = null;
		delete_count = 0;
	}

return 0;
}