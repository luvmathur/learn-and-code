import java.util.*;

class Statistics{

  int max(int a,int b){
    if(a>b){
      return 1;
    }
    else if(a==b){
      return 2;
    }
    else{
      return 3;
    }
  } 

  public String[] lexographic_sort(String [] game_array,int game_array_index){
    for(int index = 0; index < game_array_index-1; ++index){
          for (int k = index + 1; k < game_array_index; ++k){
              if (game_array[index].compareTo(game_array[k]) >= 0){
                  String temp = game_array[index];
                  game_array[index] = game_array[k];
                  game_array[k] = temp;
              }
          }
    }
    return game_array;
  }
  public String get_max_game_count(String [] game_array, int game_array_index,Statistics statistics){
    int maximum_count_of_any_game=0,current_game_count=0;
    String current_maximum_count_game="";
    for(int index=0;index<game_array_index;index++){
      String search_game = game_array[index];
      for(int index_count=0;index_count<game_array_index;index_count++){
        if(game_array[index_count].equals(search_game)){
          current_game_count++;
        }
      }
      if(statistics.max(current_game_count,maximum_count_of_any_game)==1){
        maximum_count_of_any_game=current_game_count;
        current_maximum_count_game=search_game;
      }
      else if(statistics.max(current_game_count,maximum_count_of_any_game)==2){
        if(current_maximum_count_game.compareTo(search_game)>0){
          current_maximum_count_game=search_game;
        }
      }
      current_game_count=0;
    }
    return current_maximum_count_game;
  }

  int get_football_count(HashMap<String,String> stats){
    int football_count=0;
    for(Map.Entry map:stats.entrySet()){
      if(map.getValue().toString().equals("football")){
        football_count++;
      }
    }
    return football_count;
  }

  public static void main(String args[]){
   
    Statistics statistics=new Statistics();
    HashMap<String,String> stats=new HashMap<String,String>();
    String [] game_array =new String[1000000];
    Scanner sc=new Scanner(System.in);
    int number_of_test_cases=sc.nextInt();
    int game_array_index=0;

    for(int test_case_count=0;test_case_count<number_of_test_cases;test_case_count++){
      String name = sc.next();
      String game = sc.next();
      if(stats.put(name,game)==null){
        game_array[game_array_index++]=game;
      } 
    }

    game_array = statistics.lexographic_sort(game_array,game_array_index);
    
    int football_count = statistics.get_football_count(stats);
    String maximum_count_of_any_game = statistics.get_max_game_count(game_array,game_array_index,statistics);

    System.out.println(maximum_count_of_any_game);
    System.out.println(football_count);
  }
}
