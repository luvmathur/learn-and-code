from sys import stdin

class Statistics:

    def __init__(self):
        self.player_names_list = []
        self.sport_liked_list = []
        self.sport_count_list = []
        self.football_count = 0
        self.distinct_sports = []
        self.maximum_sports_count = 0
        self.maximum_input_length = 11

    def get_player_names_and_sport_liking(self):
        number_of_entries = int(input())
        for entry in range(number_of_entries):
            player_name, sport_liked = stdin.readline().split()
            if len(player_name) <=self.maximum_input_length and len(sport_liked)<=self.maximum_input_length:
                if player_name not in self.player_names_list:
                    self.player_names_list.append(player_name)
                    if sport_liked != "football":
                        self.sport_liked_list.append(sport_liked)
                    else:
                        self.football_count = self.football_count + 1

    def get_maximum_sport_liked(self):
        for sport in self.sport_liked_list:
            if sport not in self.distinct_sports:
                self.distinct_sports.append(sport)

        self.distinct_sports = sorted(self.distinct_sports, reverse=False)
        game = ''
        for sport in self.distinct_sports:
            if list.count(self.sport_liked_list, sport) > self.maximum_sports_count:
                self.maximum_sports_count = list.count(self.sport_liked_list, sport)
                game = sport

            if list.count(self.sport_liked_list, sport) == self.maximum_sports_count:
                pass

            if self.maximum_sports_count < self.football_count:
                game = 'football'

        print(game)
        print(self.football_count)


if __name__ == '__main__':
    statistics = Statistics()
    statistics.get_player_names_and_sport_liking()
    statistics.get_maximum_sport_liked()


