#include<iostream>
#include<string>
#include<stddef.h>

using namespace std;

struct personDetail {
        string gameName;
        int gameHash;
        int gameCount ;
};

int personRecord[100000];
personDetail sportCount[100000];

int getHash(string str){
    int i, sum = 0, hashCode;

    for (unsigned int i = 0; i < str.size(); i++) {
        sum += str[i];
    }
    
    hashCode = sum*str.size();
    hashCode = hashCode%383;

    return hashCode;
}

int getCombinedHash(string name, string sport){

    int i, j, nameSum=0, sportSum=0, combineHashCode;

    for (unsigned int i = 0; i < name.size(); i++) {
        nameSum += name[i];
    }

    for (unsigned int j = 0; j < sport.size(); j++) {
        sportSum += sport[j];
    }
    combineHashCode = (nameSum*name.size())*(sportSum*sport.size());
    combineHashCode = combineHashCode % 97; 
    
    return combineHashCode;
}

int validateValues(int combinedHash){
    int index;
    for(index=0; index< sizeof(personRecord); index++){
        if(personRecord[index] == combinedHash){
            return 0;
        }
        else
            return 1;
    }
}

void getPersonDetails(int personCounter){

    std::string personName, sportName;
    int sportHash, validity=0, nameSportHash;
    
    std::cin>>personName >> sportName;
    nameSportHash = getCombinedHash(personName , sportName);
    sportHash = getHash(sportName);
    
    validity = validateValues(nameSportHash);
    if(validity == 1){
    	
        personRecord[personCounter] = nameSportHash;
                        
	    if(sportCount[sportHash].gameHash  == 0){
	
	        sportCount[sportHash].gameName= sportName;
	        sportCount[sportHash].gameHash= sportHash;
	        sportCount[sportHash].gameCount= 1;    
	    }
        else{
            sportCount[sportHash].gameCount++;
        }
    }
	return ;
}

string getPopularSport(){

    int maxLike= 0, i;
    string popularSport;

    for(i=0; i<sizeof(sportCount);i++){

        if(sportCount[i].gameCount>=maxLike){
            maxLike= sportCount[i].gameCount;
            popularSport= sportCount[i].gameName;
        }
    }
    return popularSport;
}

int getLikesforsport(string sportName){
	
    int getHashForSport, likeCount=0;
    getHashForSport= getHash(sportName);
    likeCount= sportCount[getHashForSport].gameCount;

    return likeCount;
}

int main()
{
    int totalPersons,personCounter, likesForFootball=0;
    string popularSport, fotballLike = "football";

    cin>> totalPersons;
 
    for(personCounter=0;personCounter<totalPersons;personCounter++){
 
        getPersonDetails(personCounter);
    }

    popularSport = getPopularSport();
    likesForFootball = getLikesforsport(fotballLike);
    cout<<popularSport << endl;
    cout<<likesForFootball;

    return 1;   
}
