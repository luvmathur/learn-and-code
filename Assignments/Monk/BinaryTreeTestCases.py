import unittest

from binary_search_tree import Node, BinarySearchTree


class BstTest(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(BstTest, self).__init__(*args, **kwargs)
        self.tree = BinarySearchTree()
        self.list = [100, 20, 500, 30, 10, 40, 50]

        for data in self.list:
            self.tree.insert(data)

    def test_search(self):
        self.assertTrue(self.tree.search(self.tree.get_root(), 30))
        self.assertFalse(self.tree.search(self.tree.get_root(), 12312))
        self.assertTrue(self.tree.search(self.tree.get_root(), 500))
        self.assertTrue(self.tree.search(self.tree.get_root(), 40))
        self.assertFalse(self.tree.search(self.tree.get_root(), 0))
        self.assertTrue(self.tree.search(self.tree.get_root(), 40))

    def test_height(self):
        self.assertEqual(self.tree.height(self.tree.get_root()), 5)

    def test_get_min(self):
        self.assertEqual(self.tree.get_min(self.tree.get_root()), 10)

    def test_get_max(self):
        self.assertEqual(self.tree.get_max(self.tree.get_root()), 500)


if __name__ == '__main__':
    unittest.main()
