class Node(object):
    def __init__(self, data=None, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right


class BinarySearchTree(object):
    def __init__(self, root=None):
        self.root = root

    def get_root(self):
        return self.root

    def insert(self, item):
        if self.root is None:
            self.root = Node(item)
        else:
            root = self.root
            while root is not None:
                if item < root.data:
                    if root.left is None:
                        root.left = Node(item)
                        return
                    else:
                        root = root.left
                else:
                    if root.right is None:
                        root.right = Node(item)
                        return
                    else:
                        root = root.right

    def search(self, node, item):
        if node is None:
            return False
        else:
            if node.data == item:
                return True
            elif node.data < item:
                return self.search(node.right, item)
            else:
                return self.search(node.left, item)

    def height(self, node):
        if not node:
            return 0
        else:
            return 1 + max(self.height(node.left), self.height(node.right))


    def get_minimum_value(self, root):

        while root.left:
            root = root.left
        return root.data

    def get_maximum_value(self, root):
        while root.right:
            root = root.right
        return root.data


if __name__ == '__main__':
    tree = BinarySearchTree()
    inputs = int(input().strip())
    a = [tree.insert(int(data)) for data in input().strip().split(" ")]
    print(tree.height(tree.get_root()))
