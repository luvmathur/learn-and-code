class Node():
    def __init__(self, value):
        self.data = value
        self.left = None
        self.right = None


class Tree():
    def __init__(self):
        self.root = None

    def _add(self, node, value):
        while (True):
            if value > node.data:
                if node.left is None:
                    node.left = Node(value)
                    break
                else:
                    node = node.left
            else:
                if node.right is None:
                    node.right = Node(value)
                    break
                else:
                    node = node.right

    def add(self, value):
        if self.root is None:
            self.root = Node(value)
        else:
            self._add(self.root, value)

    def height(self, node):
        if node is None:
            return 0
        else:
            return max(self.height(node.left) + 1, self.height(node.right) + 1)

    def getroot(self):
        return self.root


if __name__ == '__main__':
    tree = Tree()
    inputs = int(input().strip())
    a = [tree.add(int(data)) for data in input().strip().split(" ")]
    print(tree.height(tree.getroot()))
