class Node:
    def __init__(self, key, parent):
        self.key = key
        self.parent = parent
        self.left = None
        self.right = None


class BinaryTree():
    def __init__(self):
        self.root = None
        self.size = 0
        self.max_height = 0

    def insert_iterative(self, key):
        height = 1
        if (self.root == None):
            self.root = Node(key, None)
            self.max_height = 1
            return self.root
        current_root = self.root
        while (current_root != None):
            parent = current_root
            if (key <= current_root.key):
                current_root = current_root.left
            else:
                current_root = current_root.right
            height += 1
        child = Node(key, parent)
        if (key <= parent.key):
            parent.left = child
        else:
            parent.right = child
        if (height > self.max_height):
            self.max_height = height
        return child

if __name__=='__main__':
    key = input()
    binary_tree = BinaryTree()
    for key in input().split():
        binary_tree.insert_iterative(int(key))
    print(binary_tree.max_height)
