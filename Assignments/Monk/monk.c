#include<stdio.h>
#include<stdlib.h>
# define null '\0'

typedef struct SpiderQueue{
	struct SpiderQueue *next;
	int initial_power;
	int position;
}*queue;
struct SpiderQueue *head='\0';
struct SpiderQueue *pointer='\0';

void push(int power,int position){

	struct SpiderQueue *tmp=malloc(sizeof(queue));
	tmp->next=null;
	if(power<0){
		power = 0;
	}
	tmp->initial_power=power;
	tmp->position=position;

	if(head==null){
		head=tmp;
		pointer=head;
	}
	else{
		pointer->next=tmp;
		pointer=pointer->next;
	}
}

struct SpiderQueue *pop(){
	if(head==null){
		return;
	}
	else{
		struct SpiderQueue *tmp=head;
		head=head->next;
		tmp->next=null;
		return tmp;
	}
}

int find_max_position(struct SpiderQueue *head,int spider_selected,int number_of_spider){
	int spider_counter=0,max=-99999,position=0;
	struct SpiderQueue *tmp = head;
	for(spider_counter=0;spider_counter<spider_selected;spider_counter++){
		if(tmp){
			if(tmp->initial_power>max){
				max = tmp->initial_power;
				position = tmp->position;
			}
			tmp=tmp->next;
		}
	}
	return position;
}


int main(){
	
	int number_of_spider,spider_selected,spider_counter,spider_pop_counter;
	scanf("%d%d",&number_of_spider,&spider_selected);
	int power=0;
	for(spider_counter=1;spider_counter<=number_of_spider;spider_counter++){
		scanf("%d",&power);
		push(power,spider_counter);
	}
	for(spider_counter=0;spider_counter<spider_selected;spider_counter++){ //number of iterations
		int total_pop_needed=spider_selected;
		if(total_pop_needed>number_of_spider){
			total_pop_needed=number_of_spider;
		}
		int max_position = find_max_position(head,total_pop_needed,number_of_spider);
		printf("%d ",max_position);
		for(spider_pop_counter=0;spider_pop_counter<total_pop_needed;spider_pop_counter++){ //number of pop
			struct SpiderQueue *tmp = pop();
			if(tmp->position != max_position){
				push(tmp->initial_power-1,tmp->position);
			}
		}
		number_of_spider--;
	}	
	return 0;
}
